import sys
import justext
import requests

"""
['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', 
'__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', 
'__hash__', '__init__', '__init_subclass__', '__le__', '__len__',
 '__lt__', '__module__', '__ne__', '__new__', '__reduce__', 
 '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', 
 '__str__', '__subclasshook__', '__weakref__', 'append_text', 
 'cf_class', 'chars_count_in_links', 'class_type', 'contains_text', 
 'dom_path', 'heading', 'is_boilerplate', 'is_heading', 'links_density',
  'stopwords_count', 'stopwords_density', 'tags_count', 'text', 'text_nodes', 
  'words_count', 'xpath']
  ==============
  ['__bool__', '__class__', '__contains__', '__copy__', 
  '__deepcopy__', '__delattr__', '__delitem__', '__dict__', 
  '__dir__', '__doc__', '__eq__', '__format__', '__ge__', 
  '__getattribute__', '__getitem__', '__gt__', '__hash__',
   '__init__', '__init_subclass__', '__iter__', '__le__', 
   '__len__', '__lt__', '__module__', '__ne__', '__new__',
    '__reduce__', '__reduce_ex__', '__repr__', '__reversed__',
     '__setattr__', '__setitem__', '__sizeof__', '__str__', 
     '__subclasshook__', '__weakref__', '_init', 'addnext',
      'addprevious', 'append', 'attrib', 'base', 'base_url',
       'body', 'classes', 'clear', 'cssselect', 'drop_tag', 
       'drop_tree', 'extend', 'find', 'find_class', 
       'find_rel_links', 'findall', 'findtext', 'forms', 
       'get', 'get_element_by_id', 'getchildren', 
       'getiterator', 'getnext', 'getparent', 'getprevious', 
       'getroottree', 'head', 'index', 'insert', 'items', 
       'iter', 'iterancestors', 'iterchildren', 
       'iterdescendants', 'iterfind', 'iterlinks', 
       'itersiblings', 'itertext', 'keys', 'label',
        'make_links_absolute', 'makeelement', 'nsmap',
         'prefix', 'remove', 'replace', 'resolve_base_href',
          'rewrite_links', 'set', 'sourceline', 'tag', 
          'tail', 'text', 'text_content', 'values', 'xpath']
"""

url = sys.argv[1]

response = requests.get(url)
paragraphs = justext.justext(response.content,justext.get_stoplist("English"))

def preview_paragraphs(paragraphs):
    for i in paragraphs:
        if i.is_heading:
            print("==========================")
            print(type(i))
            print(i.is_heading)
            current_text = i.text
            i.adjust_text(f"<TITLE>{current_text}</TITLE>")
            print(i.text)
            print(i.is_boilerplate)
            print("===========================")

preview_paragraphs(paragraphs)

